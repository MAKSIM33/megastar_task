from django.db import models
from django.contrib.contenttypes.fields import GenericRelation


class BaseModel(models.Model):

    at_created = models.DateTimeField(auto_now_add=True)
    at_modified = models.DateTimeField(auto_now=True)

    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Book(BaseModel):
    author = models.ForeignKey('Writer', on_delete=models.PROTECT)
    name = models.CharField(max_length=60)


class Writer(BaseModel):
    name = models.CharField(max_length=60)
    books = GenericRelation(Book, object_id_field='author_id', content_type_field='author')


