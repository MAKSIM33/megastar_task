import psycopg2
import os
import dotenv
dotenv.load_dotenv()


def get_data(writer_id):
    connection = psycopg2.connect(user=os.getenv('DB_USER'),
                                  password=os.getenv('DB_PASSWORD'),
                                  host=os.getenv('DB_HOST'),
                                  port=os.getenv('DB_PORT'),
                                  database=os.getenv('DB_NAME'))
    cursor = connection.cursor()
    insert_query = f"select json_build_object('id', {writer_id}, " \
                   f"'name', (select name from writers_writer where id='{writer_id}')," \
                   f"'books', json_agg(json_build_object('id', writers_book.id, 'name', writers_book.name))) " \
                   f"from writers_book where author_id='{writer_id}';"
    cursor.execute(insert_query)
    result = cursor.fetchall()[0][0]
    return result
