from django.http import JsonResponse, HttpResponse
from http import HTTPStatus
from writers.services import get_data


def main(request, writer_id):
    if request.method == 'GET':
        return JsonResponse(get_data(writer_id))
    return HttpResponse('method not allowed', status=HTTPStatus.METHOD_NOT_ALLOWED)
