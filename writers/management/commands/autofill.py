from django.core.management.base import BaseCommand, CommandError
from writers.models import Book, Writer


class Command(BaseCommand):
    help = 'Loading test data into tables'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        writer1 = Writer.objects.create(name='Лев Толстой')
        writer2 = Writer.objects.create(name='Александр Пушкин')
        Writer.objects.create(name='Михаил Лермонтов')
        Book.objects.create(name='Война и мир', author=writer1)
        Book.objects.create(name='Воскресенье', author=writer1)
        Book.objects.create(name='Евгений Онегин', author=writer2)
        Book.objects.create(name='Моцарт и Сальери', author=writer2)

