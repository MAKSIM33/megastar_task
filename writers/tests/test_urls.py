from django.test import SimpleTestCase
from django.urls import reverse, resolve

from writers.views import main


class TestUrls(SimpleTestCase):

    def test_main_urls_resolves(self):
        url = reverse('main', args=['1'])
        self.assertEquals(resolve(url).func, main)

