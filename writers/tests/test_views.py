from django.test import TestCase, Client
from django.urls import reverse
from writers.models import Writer, Book


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.main_url = reverse('main', args=['1'])
        writer = Writer.objects.create(
            name='Лев Толстой'
        )
        Book.objects.create(
            name='Война и мир',
            author=writer
        )
        Book.objects.create(
            name='Воскресенье',
            author=writer
        )
        writer = Writer.objects.create(
            name='Александр Пушкин'
        )
        Book.objects.create(
            name='Евгений Онегин',
            author=writer
        )


    def build_expected(self, writer, books):
        books = []
        return {}


    def test_data_and_status_main_GET(self):
        response = self.client.get(self.main_url)
        self.assertEquals(response.status_code, 200)
        book = Book.objects.filter(author__id='1')
        self.assertEquals(book[0].author.name, 'Лев Толстой')
        self.assertEquals(book[0].name, 'Война и мир')
        self.assertEquals(book[1].name, 'Воскресенье')

