# Megastar task

Для запуска необходимо заполнить конфигурационный файл **.env**

## Команды запуска:

`pip install virtualenv`

`virtualenv env`

`source env/bin/activate`

`pip install -r requirements.txt`

`python manage.py makemigrations`

`python manage.py migrate`

`python manage.py test writers` - запуск тестов при необходимости, нужно, чтоб пользователь бд имел права на создание тестовой бд

`python manage.py autofill` - с помощью этой команды можно заполнить бд тестовыми данными

`python manage.py runserver`

Админ панель выключена, если она потребуется нужно раскомментировать строку с импортом и url в файле `test_task/urls.py` и создать пользователя командой:

`python manage.py createsuperuser`